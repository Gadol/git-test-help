# Documentation development workflow overview

  + [Workflow](#workflow)
  + [Gitlab overview](#gitlab-overview)
  + [Labels and milestones](#labels-and-milestones)
  + [Issues](#issues)
  + [Merge Requests](#merge-requests)

Welcome to The Company!

Here you can access to our documentation development workflow.

You are familiar with basic Git commands already, so you are ready to meet with our 
workflow closer.

We use `GitLab` as a project management system both for code and documentation.
If you are reading this article, you are already registered in our `GitLab` server and 
ready for work.

## Workflow

We use some kind of a trunk-based development style as our workflow.

There is the main `master` branch, which is default to show for documentation readers.

To make changes, you should create a feature branch, which aims to solve any finite 
task. After the task is completed, the feature branch must be merged into the `master` . 

In case of long-time feature development, the `master` branch should be merged into the 
feature branch frequently to keep up to date.
An illustration of this workflow you can see in the image below.

![workflow](img/workflow.svg)

## Gitlab overview

At first, you should know, that projects with code and docs are divided into groups for 
purpose of permission accessibility administration.

Each code project has an adjacent documentation project. Your main task is keeping 
up-to-date documentation projects.

Usually, you will have `Developer` or `Maintainer` access rights in documentation 
projects according to your level. Also, you will have `Reporter` access to code projects 
to browse code freely.

More details about GitLab access permissions you can find 
[here](https://docs.gitlab.com/ee/user/permissions.html).

## Labels and milestones

Our working process is divided into two-week sprints. 

All tasks (issues) can be grouped 
to the current sprint, product backlog, and technical debt.

Sprint is set by Milestone in `GitLab` .

All issues and merge requests should have specific labels for:

* class
* priority
* category 
* status

All labels are [here](https://gitlab.com/Gadol/git-test-help/-/labels).

## Issues

And so, you have your documentation project.

As a sprint starts, you should plan your tasks for the next two weeks with your team 
lead.
The team lead will propose an initial plan according to priority. You can offer your 
suggestions. Anyway, you must have your task pool for the current sprint.

Pay attention, that initially planned tasks are would be done, if there won't be more 
priority tasks, which could appear during a sprint. If this happened, you must inform 
your team lead to correct your plan to avoid an undone task situation.

### Issue assign example

Go to the issue panel of your project, find issues with the `product_backlog` label and 
choose the highest priority

![](img/choose_backlog_issue.gif)

Assign the issue to yourself, remove the `product_backlog` label, and set Milestone to 
the current sprint

![](img/assign_issue.gif)

#### Tip!

It is very useful to create a specific branch for one issue and name it with the next 
pattern:

` `  ` <issue_number>-<branch_name> `  ` ` . 

It will allow to automatically close an issue when the related branch will be merged to 
the `master` .
For example, if you have an issue like in the picture below

![](img/issue_number.png)

You can create a branch in the next convenient way:

``` bash
git checkout -b 1-commit_pattern_tip
```

## Merge Requests

If you think, that a task is done and all tests were passed correctly, you must merge 
your changes into the `master` branch.

If you have a `Maintainer` access level, you could merge your feature branch by 
yourself. Unless you should create a Merge Request and assign it to a project 
`Maintainer` . 

The `Maintainer` will review your changes and will merge them or will 
give you feedback and reassign the merge request back to you. You must resolve all 
mistakes and assign the merge request back to the `Maintainer` . 
This cycle would be 
repeated until all mistakes will be resolved.

### Merge exmaple

Go to the commits panel of your projects, click on `create merge request` button. Check 
if there a right direction of merge (from which branch to which)

![](img/create_merge_request.gif)

Next, assign a merge request yourself, set a milestone, 
and check the `Delete source branch` checkbox. Merge request is created!

![](img/create_merge_request-2.gif)

Check if all tests are passed (there no tests in the example) and click the `merge` 
button
![](img/merged.gif)
